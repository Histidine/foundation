package com.fs.starfarer.api.impl.campaign.rulecmd;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.BattleAPI;
import com.fs.starfarer.api.campaign.CampaignEventListener;
import com.fs.starfarer.api.campaign.CampaignFleetAPI;
import com.fs.starfarer.api.campaign.InteractionDialogAPI;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.campaign.rules.MemoryAPI;
import com.fs.starfarer.api.characters.OfficerDataAPI;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.impl.campaign.FleetEncounterContext;
import com.fs.starfarer.api.util.Misc;
import java.util.List;
import java.util.Map;
import org.histidine.foundation.scripts.UtilFuncs;

public class Foundation_MergeIntoPlayerFleet extends BaseCommandPlugin {
		
	@Override
	public boolean execute(String ruleId, InteractionDialogAPI dialog, List<Misc.Token> params, Map<String, MemoryAPI> memoryMap) {
		
		SectorEntityToken target = dialog.getInteractionTarget();
		if (target instanceof CampaignFleetAPI) {
			CampaignFleetAPI fleet = (CampaignFleetAPI)target;
			CampaignFleetAPI playerFleet = Global.getSector().getPlayerFleet();
			
			List<OfficerDataAPI> officers = fleet.getFleetData().getOfficersCopy();
			for (OfficerDataAPI officer : officers) {
				playerFleet.getFleetData().addOfficer(officer.getPerson());
				fleet.getFleetData().removeOfficer(officer.getPerson());
				UtilFuncs.purgeNonCombatSkills(officer.getPerson());
			}
			
			List<FleetMemberAPI> members = fleet.getFleetData().getMembersListCopy();
			for (FleetMemberAPI member : members) {
				playerFleet.getFleetData().addFleetMember(member);
				playerFleet.getCargo().addCrew((int)member.getMinCrew());
				fleet.getFleetData().removeFleetMember(member);
				
				int crew = (int)Math.min(member.getNeededCrew() * 1.5f, member.getMaxCrew());
				int supplies = (int)member.getCargoCapacity()/2;
				int fuel = (int)Math.min(member.getFuelUse() * 20, member.getFuelCapacity());
				
				playerFleet.getCargo().addCrew(crew);
				playerFleet.getCargo().addSupplies(supplies);
				playerFleet.getCargo().addFuel(fuel);
			}
			
			fleet.despawn(CampaignEventListener.FleetDespawnReason.REACHED_DESTINATION, null);
			showFleetInfo(dialog, playerFleet, fleet);
			return true;
		}
		return false;
	}
	
	protected void showFleetInfo(InteractionDialogAPI dialog, CampaignFleetAPI player, CampaignFleetAPI other) {
		BattleAPI b = player.getBattle();
		if (b == null) b = other.getBattle();
		if (b != null && b.isPlayerInvolved()) {
			String titleOne = "Your forces";
			if (b.isPlayerInvolved() && b.getPlayerSide().size() > 1) {
				titleOne += ", with allies";
			}
			if (!Global.getSector().getPlayerFleet().isValidPlayerFleet()) {
				titleOne = "Allied forces";
			}
			String titleTwo = null;
			if (b.getPrimary(b.getNonPlayerSide()) != null) {
				titleTwo = b.getPrimary(b.getNonPlayerSide()).getNameWithFactionKeepCase();
			}
			if (b.getNonPlayerSide().size() > 1) titleTwo += ", with allies";
			dialog.getVisualPanel().showFleetInfo(titleOne, b.getPlayerCombined(), Misc.ucFirst(titleTwo), b.getNonPlayerCombined(), null);
		} else {
			if (b != null) {
				String titleOne = b.getPrimary(b.getSideOne()).getNameWithFactionKeepCase();
				if (b.getSideOne().size() > 1) titleOne += ", with allies";
				String titleTwo = b.getPrimary(b.getSideTwo()).getNameWithFactionKeepCase();
				if (b.getSideTwo().size() > 1) titleTwo += ", with allies";
				
				FleetEncounterContext fake = new FleetEncounterContext();
				fake.setBattle(b);
				dialog.getVisualPanel().showPreBattleJoinInfo(null, player, Misc.ucFirst(titleOne), Misc.ucFirst(titleTwo), fake);
			} else {
				dialog.getVisualPanel().showFleetInfo((String)null, player, (String)null, other, null);
			}
		}
	}
}