package com.fs.starfarer.api.impl.campaign.rulecmd;

import java.util.List;
import java.util.Map;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.CargoAPI;
import com.fs.starfarer.api.campaign.CargoStackAPI;
import com.fs.starfarer.api.campaign.FactionAPI;
import com.fs.starfarer.api.campaign.InteractionDialogAPI;
import com.fs.starfarer.api.campaign.RepLevel;
import com.fs.starfarer.api.campaign.ReputationActionResponsePlugin.ReputationAdjustmentResult;
import com.fs.starfarer.api.campaign.TextPanelAPI;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.campaign.rules.MemKeys;
import com.fs.starfarer.api.campaign.rules.MemoryAPI;
import com.fs.starfarer.api.combat.WeaponAPI;
import com.fs.starfarer.api.combat.WeaponAPI.AIHints;
import com.fs.starfarer.api.combat.WeaponAPI.WeaponSize;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.fleet.FleetMemberType;
import com.fs.starfarer.api.fleet.ShipRolePick;
import com.fs.starfarer.api.impl.campaign.CoreReputationPlugin.CustomRepImpact;
import com.fs.starfarer.api.impl.campaign.CoreReputationPlugin.RepActionEnvelope;
import com.fs.starfarer.api.impl.campaign.CoreReputationPlugin.RepActions;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.impl.campaign.ids.ShipRoles;
import com.fs.starfarer.api.loading.WeaponSlotAPI;
import com.fs.starfarer.api.loading.WeaponSpecAPI;
import com.fs.starfarer.api.util.Misc;
import com.fs.starfarer.api.util.Misc.Token;
import com.fs.starfarer.api.util.MutableValue;
import com.fs.starfarer.api.util.WeightedRandomPicker;
import java.awt.Color;
import java.util.Random;
import org.histidine.foundation.scripts.StringHelper;

/**
 *	DonateToFoundation <credits>
 */
public class DonateToFoundation extends BaseCommandPlugin {
	public static final float REPUTATION_PER_10K_CREDITS = 0.01f;
	public static final float REPUTATION_MARKET_FACTION_MULT = 2;
	public static final float REPUTATION_BONUS_MULT_OVER_50K = 1.2f;
	public static final float LARGE_DONATION_THRESHOLD = 50000;
	
	public Random random = new Random();

	public boolean execute(String ruleId, InteractionDialogAPI dialog, List<Token> params, Map<String, MemoryAPI> memoryMap) {
		if (dialog == null) return false;
		
		CargoAPI cargo = Global.getSector().getPlayerFleet().getCargo();
		MutableValue credits = cargo.getCredits();
		float donation = params.get(0).getFloat(memoryMap);
		float currentCredits = credits.get();
		if (donation > currentCredits) return false;
		
		float baseRep = REPUTATION_PER_10K_CREDITS * donation/10000;
		WeaponSpecAPI freeWeapon = null;
		boolean largeDonation = donation >= LARGE_DONATION_THRESHOLD;
		if (largeDonation)
		{
			baseRep *= REPUTATION_BONUS_MULT_OVER_50K;
		}
		freeWeapon = getRandomWeapon(dialog.getInteractionTarget().getMarket(), largeDonation);
		
		String factionId = dialog.getInteractionTarget().getFaction().getId();
		boolean isIndieMarket = factionId.equals(Factions.INDEPENDENT);
		
		// probably a bad idea to affect indies unless specifically targeted
		// given the commission/hostility mechanics
		/*
		CustomRepImpact impactIndies = new CustomRepImpact();
		if (isIndieMarket)
			impactIndies.delta = baseRep + baseRep * REPUTATION_MARKET_FACTION_MULT;
		else
			impactIndies.delta = baseRep;
		impactIndies.limit = RepLevel.WELCOMING;
		ReputationActionResponsePlugin.ReputationAdjustmentResult result = Global.getSector().adjustPlayerReputation(
				new RepActionEnvelope(RepActions.CUSTOM, impactIndies, null, true), Factions.INDEPENDENT);
		*/
		
		CustomRepImpact impactFaction = new CustomRepImpact();
		impactFaction.delta = baseRep * REPUTATION_MARKET_FACTION_MULT;
		impactFaction.limit = RepLevel.WELCOMING;
		ReputationAdjustmentResult resultFaction = Global.getSector().adjustPlayerReputation(
				new RepActionEnvelope(RepActions.CUSTOM, impactFaction, dialog.getTextPanel()), factionId);
		
		CustomRepImpact impactPerson = new CustomRepImpact();
		impactPerson.delta = baseRep * REPUTATION_MARKET_FACTION_MULT;
		//impactPerson.limit = RepLevel.WELCOMING;
		ReputationAdjustmentResult resultPerson = Global.getSector().adjustPlayerReputation(
				new RepActionEnvelope(RepActions.CUSTOM, impactPerson, dialog.getTextPanel()), dialog.getInteractionTarget().getActivePerson());
		
		credits.add(-1 * (int)donation);
		if (freeWeapon != null) 
			cargo.addWeapons(freeWeapon.getWeaponId(), 1);
		
		TextPanelAPI text = dialog.getTextPanel();
		AddRemoveCommodity.addCreditsLossText((int)donation, text);
		if (freeWeapon != null)
		{
			String str = StringHelper.getStringAndSubstituteToken("foundation", "addedWeapon", "$weapon", freeWeapon.getWeaponName());
			str = Misc.ucFirst(str);
			text.setFontSmallInsignia();
			text.addParagraph(str, Misc.getPositiveHighlightColor());
			text.highlightFirstInLastPara(freeWeapon.getWeaponName(), Misc.getHighlightColor());
			text.setFontInsignia();
		}
		
		MemoryAPI memory = Global.getSector().getCharacterData().getMemory();
		memory.set("$credits", (int)credits.get(), 0);
		memory.set("$creditsStr", Misc.getWithDGS(credits.get()), 0);
		
		return true;
	}
	
	protected WeaponSpecAPI getRandomWeapon(MarketAPI market, boolean largeDonation) {
		if (market == null) return null;
		float qf = market.getShipQualityFactor();
		
		WeightedRandomPicker<WeaponSpecAPI> picker = new WeightedRandomPicker<>();
		
		FactionAPI faction = market.getFaction();
		int tries = 0;
		while (picker.isEmpty() && tries < 10)
		{
			List<ShipRolePick> picks = faction.pickShip(pickRandomRole(market.getSize()), qf, random);
			for (ShipRolePick pick : picks) {
				FleetMemberType type = FleetMemberType.SHIP;
				if (pick.isFighterWing()) {
					//type = FleetMemberType.FIGHTER_WING;
					continue;
				}

				FleetMemberAPI member = Global.getFactory().createFleetMember(type, pick.variantId);

				for (String slotId : member.getVariant().getFittedWeaponSlots()) {
					WeaponSlotAPI slot = member.getVariant().getSlot(slotId);
					if (slot.isBuiltIn() || slot.isSystemSlot() || slot.isDecorative()) continue;
					if (largeDonation && slot.getSlotSize() != WeaponSize.MEDIUM) continue;
					else if (!largeDonation && slot.getSlotSize() != WeaponSize.SMALL) continue;
					WeaponSpecAPI spec = member.getVariant().getWeaponSpec(slotId);
					if (spec.getAIHints().contains(AIHints.SYSTEM)) continue;
					picker.add(spec);
				}
			}
			tries++;
		}
		return picker.pick();
	}
	
	protected String pickRandomRole(int marketSize)
	{
		WeightedRandomPicker<String> picker = new WeightedRandomPicker<>();
		picker.add(ShipRoles.COMBAT_SMALL, 3);
		picker.add(ShipRoles.ESCORT_SMALL);
		picker.add(ShipRoles.FREIGHTER_SMALL);
		picker.add(ShipRoles.COMBAT_FREIGHTER_SMALL);
		picker.add(ShipRoles.CARRIER_SMALL);
		if (marketSize >= 2) {
			picker.add(ShipRoles.COMBAT_MEDIUM, 2);
			picker.add(ShipRoles.COMBAT_MEDIUM, 2);
			picker.add(ShipRoles.FREIGHTER_MEDIUM);
			picker.add(ShipRoles.COMBAT_FREIGHTER_MEDIUM);
			picker.add(ShipRoles.CARRIER_MEDIUM);
		}
		if (marketSize >= 4) {
			picker.add(ShipRoles.CARRIER_LARGE);
			picker.add(ShipRoles.FREIGHTER_LARGE);
			picker.add(ShipRoles.COMBAT_FREIGHTER_LARGE);
			picker.add(ShipRoles.COMBAT_LARGE, 2);
		}
		if (marketSize >= 6) {
			picker.add(ShipRoles.CARRIER_LARGE);
			picker.add(ShipRoles.COMBAT_CAPITAL, 2);
		}
		return picker.pick();
	}

}
