package org.histidine.foundation.campaign.missions;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.campaign.events.CampaignEventManagerAPI;
import com.fs.starfarer.api.campaign.events.CampaignEventPlugin;
import com.fs.starfarer.api.campaign.events.CampaignEventTarget;
import com.fs.starfarer.api.characters.PersonAPI;
import com.fs.starfarer.api.impl.campaign.CoreReputationPlugin;
import com.fs.starfarer.api.impl.campaign.CoreReputationPlugin.CustomRepImpact;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.impl.campaign.missions.BaseCampaignMission;
import org.histidine.foundation.scripts.StringHelper;

@Deprecated
public class TutorialMission extends BaseCampaignMission {
	
	public static final String PERSON_CHECKOUT_REASON = "foundation_tutorial_mission_contact";
	
	protected MarketAPI market;
	protected MarketAPI marketStealth;
	protected MarketAPI marketPirate;
	protected MarketAPI marketBounty;
	protected MarketAPI marketBountyLoc;
	protected PersonAPI contactStart;
	protected PersonAPI contactStealth;
	protected PersonAPI contactCombat;
	protected PersonAPI contactPirate;
	protected PersonAPI contactBounty;
	protected PersonAPI recruit;	// not used
	protected String recruit_shipVariant;	// is used
	
	protected PersonAPI importantPerson;
	
	
	public TutorialMission(TutorialMissionArgs args) {
		
		market = args.market;
		marketStealth = args.marketStealth;
		marketPirate = args.marketPirate;
		marketBounty = args.marketBounty;
		marketBountyLoc = args.marketBountyLoc;
		contactStart = args.contactStart;
		contactStealth = args.contactStealth;
		contactCombat = args.contactCombat;
		contactPirate = args.contactPirate;
		contactBounty = args.contactBounty;
		recruit = args.recruit;
		recruit_shipVariant = args.recruit_shipVariant;
		importantPerson = contactStart;
		
		CampaignEventManagerAPI eventManager = Global.getSector().getEventManager();
		CampaignEventTarget target = new CampaignEventTarget(market);
		event = eventManager.primeEvent(target, "foundation_tutorial", this);
		
		Global.getSector().getImportantPeople().checkOutPerson(args.contactStart, PERSON_CHECKOUT_REASON);
		Global.getSector().getImportantPeople().checkOutPerson(args.contactStealth, PERSON_CHECKOUT_REASON);
	}
	
	public PersonAPI getContactStart() {
		return contactStart;
	}
	
	public PersonAPI getContactStealth() {
		return contactStealth;
	}
	
	public PersonAPI getContactCombat() {
		return contactCombat;
	}
	
	public void setContactCombat(PersonAPI contact) {
		contactCombat = contact;
	}
	
	public PersonAPI getContactPirate() {
		return contactPirate;
	}
	
	public PersonAPI getContactBounty() {
		return contactBounty;
	}
	
	public void setContactBounty(PersonAPI contact) {
		contactBounty = contact;
	}
	
	public PersonAPI getRecruit() {
		return recruit;
	}
	
	public String getRecruitShipVariant() {
		return recruit_shipVariant;
	}
	
	public CustomRepImpact getRepChange(float delta) {
		CustomRepImpact impact = new CoreReputationPlugin.CustomRepImpact();
		impact.delta = delta;
		return impact;
	}
	
	@Override
	public void advance(float amount) {

	}

	@Override
	public String getName() {
		String str = StringHelper.getString("foundation_tutorial", "missionName");
		str = str + " " + StringHelper.getString("foundation_tutorial", ((TutorialMissionEvent)event).getStage());
		return str;
	}

	@Override
	public void playerAccept(SectorEntityToken entity) {
		super.playerAccept(entity);
		CampaignEventManagerAPI eventManager = Global.getSector().getEventManager();
		eventManager.startEvent(event);
	}

	public MarketAPI getMarket() {
		return market;
	}
	
	public MarketAPI getMarketStealth() {
		return marketStealth;
	}
	
	public MarketAPI getMarketPirate() {
		return marketPirate;
	}
	
	public MarketAPI getMarketBounty() {
		return marketBounty;
	}
	
	public MarketAPI getMarketBountyLoc() {
		return marketBountyLoc;
	}

	@Override
	public CampaignEventPlugin getPrimedEvent() {
		return event;
	}
	
	@Override
	public PersonAPI getImportantPerson() {
		return importantPerson;
	}
	
	public void setImportantPerson(PersonAPI person) {
		importantPerson = person;
	}

	@Override
	public String getFactionId() {
		return Factions.INDEPENDENT;
	}
	
	public static class TutorialMissionArgs {
		protected MarketAPI market;
		protected MarketAPI marketStealth;
		protected MarketAPI marketPirate;
		protected MarketAPI marketBounty;
		protected MarketAPI marketBountyLoc;
		protected PersonAPI contactStart;
		protected PersonAPI contactStealth;
		protected PersonAPI contactCombat;
		protected PersonAPI contactPirate;
		protected PersonAPI contactBounty;
		protected PersonAPI recruit;
		protected String recruit_shipVariant = "wolf_Starting";
	}
}




