package org.histidine.foundation.campaign.missions;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.Script;
import com.fs.starfarer.api.campaign.BaseOnMessageDeliveryScript;
import com.fs.starfarer.api.campaign.BattleAPI;
import com.fs.starfarer.api.campaign.CampaignFleetAPI;
import com.fs.starfarer.api.campaign.FactionAPI;
import com.fs.starfarer.api.campaign.FleetAssignment;
import com.fs.starfarer.api.campaign.InteractionDialogAPI;
import com.fs.starfarer.api.campaign.ReputationActionResponsePlugin.ReputationAdjustmentResult;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.campaign.StarSystemAPI;
import com.fs.starfarer.api.campaign.comm.CommMessageAPI;
import com.fs.starfarer.api.campaign.comm.MessagePriority;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.campaign.events.CampaignEventTarget;
import com.fs.starfarer.api.campaign.rules.MemKeys;
import com.fs.starfarer.api.campaign.rules.MemoryAPI;
import com.fs.starfarer.api.characters.FullName;
import com.fs.starfarer.api.characters.PersonAPI;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.fleet.FleetMemberType;
import com.fs.starfarer.api.impl.campaign.CoreReputationPlugin.RepActionEnvelope;
import com.fs.starfarer.api.impl.campaign.CoreReputationPlugin.RepActions;
import com.fs.starfarer.api.impl.campaign.events.BaseEventPlugin;
import com.fs.starfarer.api.impl.campaign.events.OfficerManagerEvent;
import com.fs.starfarer.api.impl.campaign.fleets.FleetFactoryV2;
import com.fs.starfarer.api.impl.campaign.fleets.FleetParams;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.impl.campaign.ids.FleetTypes;
import com.fs.starfarer.api.impl.campaign.ids.MemFlags;
import com.fs.starfarer.api.impl.campaign.ids.Personalities;
import com.fs.starfarer.api.util.IntervalUtil;
import com.fs.starfarer.api.util.Misc;
import com.fs.starfarer.api.util.Misc.Token;
import org.apache.log4j.Logger;
import org.histidine.foundation.scripts.StringHelper;
import org.lwjgl.util.vector.Vector2f;

/*
	Stages are:
	- start: start of game
	- stealth: received assignment from contact, go to Lawson's point
	- combat: successful sneak, go to asteroid clump for combat
	- end combat, win: defeated pirate fleet
	- end combat, lose: defeated by pirate fleet
	- end combat, ignored: let the ally fleet fight the pirate fleet alone
	- end combat, cancelled (enemy): pirate fleet destroyed in unrelated incident
	- end combat, cancelled (ally): ally fleet destroyed in unrelated incident
	- end, other
	- quit: abandoned tutorial mission

	- todo: bounty stage
		- change end combat win/lose/cancelled to non-end
		- add pirate notification
*/

@Deprecated
public class TutorialMissionEvent extends BaseEventPlugin {
	public static final float FP_ALLY = 12;
	public static final float FP_ENEMY = 12;
	public static final float FP_BOUNTY = 3;
	public static final float FP_BOUNTY_BACKUP = 3;
	public static final float BOUNTY_DAYS = 30 * 2;	// 2 months
	public static final float BOUNTY_AMOUNT = 15000;
	
	public static Logger log = Global.getLogger(TutorialMissionEvent.class);
	
	protected TutorialMission mission = null;

	protected float elapsedDays = 0;
	protected float elapsedDaysBounty = 0;
	protected boolean ended = false;
	protected String stage = "start";
	protected IntervalUtil interval = new IntervalUtil(0.1f,0.1f);
	protected CampaignFleetAPI fleetAlly;
	protected CampaignFleetAPI fleetEnemy;
	protected CampaignFleetAPI fleetBounty;
	protected CampaignFleetAPI fleetBountyBackup;
	protected SectorEntityToken asteroids;
	protected SectorEntityToken barad = Global.getSector().getEntityById("barad");
	protected SectorEntityToken posAlly;
	protected SectorEntityToken posEnemy;
	protected StarSystemAPI system;
	protected boolean combatActivated = false;
	protected float bountyPayment = 0;
	
	
	protected BaseOnMessageDeliveryScript deliverySuccess = new BaseOnMessageDeliveryScript() {
			public void beforeDelivery(CommMessageAPI message) {
				ReputationAdjustmentResult result = Global.getSector().adjustPlayerReputation(
						new RepActionEnvelope(RepActions.CUSTOM, mission.getRepChange(0.03f),
											  message, null, true),
											  Factions.INDEPENDENT);
				result = Global.getSector().adjustPlayerReputation(
						new RepActionEnvelope(RepActions.CUSTOM, mission.getRepChange(0.03f),
								message, null, true),
								mission.getContactCombat());
			}
		};
		
	protected BaseOnMessageDeliveryScript deliveryFailure = new BaseOnMessageDeliveryScript() {
			public void beforeDelivery(CommMessageAPI message) {
				ReputationAdjustmentResult result = Global.getSector().adjustPlayerReputation(
						new RepActionEnvelope(RepActions.CUSTOM, mission.getRepChange(-0.03f),
											  message, null, true),
											  Factions.INDEPENDENT);
				result = Global.getSector().adjustPlayerReputation(
						new RepActionEnvelope(RepActions.CUSTOM, mission.getRepChange(-0.03f),
								message, null, true),
								mission.getContactCombat());
			}
		};		
	
	@Override
	public void init(String type, CampaignEventTarget eventTarget) {
		super.init(type, eventTarget, true);
	}
	
	@Override
	public void setParam(Object param) {
		mission = (TutorialMission) param;
		updateDaysLeft();
		
		system = mission.getMarketStealth().getStarSystem();
		asteroids = system.createToken(900, 7850);
		asteroids.setCircularOrbit(system.getStar(), 140f, 7800, 400);
	}

	@Override
	public void startEvent() {
		super.startEvent();
		
		String stageId = "start";
		
		Global.getSector().reportEventStage(this, stageId, mission.getMarket().getPrimaryEntity(), MessagePriority.ENSURE_DELIVERY);
		
		//mission.getMarket().addPerson(mission.getContact());
		PersonAPI contactStart = mission.getContactStart();
		setContactFlags(contactStart, "start");
		mission.getMarket().getCommDirectory().addPerson(contactStart);
		updateDaysLeft();
		
		Vector2f l4Loc = asteroids.getLocation();
		float xAlly = l4Loc.getX() - 150;
		float yAlly = l4Loc.getY() - 150;
		posAlly = system.createToken(xAlly, yAlly);
		float orbitAngleAlly = (float)Math.atan2(yAlly, xAlly);
		posAlly.setCircularOrbit(system.getStar(), orbitAngleAlly, 7800, 400);
		
		float xEnemy = l4Loc.getX() + 150;
		float yEnemy = l4Loc.getY() + 150;
		posEnemy = system.createToken(xEnemy, yEnemy);
		float orbitAngleEnemy = (float)Math.atan2(yEnemy, xEnemy);
		posEnemy.setCircularOrbit(system.getStar(), orbitAngleEnemy, 7800, 400);
	}
	
	public void endEvent() {
		ended = true;
		// cleanup contacts

		PersonAPI contactStart = mission.getContactStart();
		unsetContactFlags(contactStart, "start");
		mission.getMarket().getCommDirectory().removePerson(contactStart);
		mission.getMarket().removePerson(contactStart);
		
		PersonAPI contactStealth = mission.getContactStealth();
		unsetContactFlags(contactStealth, "stealth");
		mission.getMarketStealth().getCommDirectory().removePerson(contactStealth);
		mission.getMarketStealth().removePerson(contactStealth);
		
		PersonAPI contactPirate = mission.getContactPirate();
		unsetContactFlags(contactPirate, "pirate");
		mission.getMarket().getCommDirectory().removePerson(contactPirate);
		mission.getMarket().removePerson(contactPirate);
		
		if (fleetAlly != null && fleetAlly.isAlive()) {
			goHome(fleetAlly, mission.getMarketStealth().getPrimaryEntity());
		}

		if (fleetEnemy != null && fleetEnemy.isAlive())
		{
			goHome(fleetEnemy, mission.getMarketPirate().getPrimaryEntity());
		}
		
		if (fleetBounty != null && fleetBounty.isAlive())
		{
			goHome(fleetBounty, mission.getMarketBounty().getPrimaryEntity());
		}
	}
	
	protected void setContactFlags(PersonAPI contact, String contactType) {
		contact.getMemoryWithoutUpdate().set("$foundation_tutorial_contact" + Misc.ucFirst(contactType), true);
		contact.getMemoryWithoutUpdate().set("$foundation_tutorial_eventRef", this);
		
		Misc.setFlagWithReason(contact.getMemoryWithoutUpdate(),
							  MemFlags.MEMORY_KEY_MISSION_IMPORTANT,
							  "foundation_tutorial", true, 999999);
	}
	
	protected void unsetContactFlags(PersonAPI contact, String contactType)
	{
		contact.getMemoryWithoutUpdate().unset("$foundation_tutorial_contact" + Misc.ucFirst(contactType));
		contact.getMemoryWithoutUpdate().unset("$foundation_tutorial_eventRef");
		Misc.setFlagWithReason(contact.getMemoryWithoutUpdate(),
				  MemFlags.MEMORY_KEY_MISSION_IMPORTANT,
				  "foundation_tutorial", false, 0f);
		
		Global.getSector().getImportantPeople().returnPerson(contact, TutorialMission.PERSON_CHECKOUT_REASON);
		if (!Global.getSector().getImportantPeople().isCheckedOutForAnything(contact)) {
			market.getCommDirectory().removePerson(contact);
			mission.getMarket().removePerson(contact);
		}
	}
	
	protected void launchRecruitFleet(MarketAPI origin)
	{
		CampaignFleetAPI fleet = FleetFactoryV2.createEmptyFleet(Factions.INDEPENDENT, FleetTypes.MERC_SCOUT, market);
		// add Wolf
		FleetMemberAPI member = Global.getFactory().createFleetMember(FleetMemberType.SHIP, mission.getRecruitShipVariant());
		fleet.getFleetData().addFleetMember(member);
		fleet.getFleetData().setFlagship(member);
		// add officer
		FactionAPI faction = Global.getSector().getFaction(Factions.INDEPENDENT);
		PersonAPI recruit = OfficerManagerEvent.createOfficer(faction, 1, false);
		recruit.getRelToPlayer().setRel(0.15f);
		fleet.getFleetData().addOfficer(recruit);
		fleet.setCommander(recruit);
		member.setCaptain(recruit);
		
		fleet.getMemoryWithoutUpdate().set("$foundation_tutorial_recruit", true);
		
		fleet.forceSync();
		SectorEntityToken originEntity = origin.getPrimaryEntity();
		origin.getPrimaryEntity().getContainingLocation().addEntity(fleet);
		fleet.setLocation(originEntity.getLocation().x, originEntity.getLocation().y);
		
		fleet.addAssignment(FleetAssignment.INTERCEPT, Global.getSector().getPlayerFleet(), 10);
		fleet.addAssignment(FleetAssignment.GO_TO_LOCATION_AND_DESPAWN, originEntity, 99999);
	}
	
	protected void goHome(CampaignFleetAPI fleet, SectorEntityToken home) {
		fleet.clearAssignments();
		fleet.addAssignment(FleetAssignment.GO_TO_LOCATION, home, 100);
		fleet.addAssignment(FleetAssignment.ORBIT_PASSIVE, home, 1);
		fleet.addAssignment(FleetAssignment.GO_TO_LOCATION_AND_DESPAWN, home, 100);
	}
	
	protected void primeCombatFleets() {
		MarketAPI marketAlly = mission.getMarketStealth();
		MarketAPI marketEnemy = mission.getMarketPirate();
		
		FleetParams fleetParamsAlly = new FleetParams(null, marketAlly, Factions.INDEPENDENT, null, FleetTypes.MERC_PRIVATEER, 
				FP_ALLY - 2, // combat
				0,		 // freighters
				0,		// tankers
				0,		// personnel transports
				0,		// liners
				0,		// civilian
				2,	// utility
				0, 0.8f, 0, 0);	// quality bonus, quality override, officer num mult, officer level bonus
		fleetAlly = FleetFactoryV2.createFleet(fleetParamsAlly);
		fleetAlly.setAIMode(true);
		fleetAlly.getMemoryWithoutUpdate().set(MemFlags.MEMORY_KEY_PATROL_FLEET, true);
		fleetAlly.getMemoryWithoutUpdate().set("$foundation_tutorial_fleetAlly", true);
		fleetAlly.getMemoryWithoutUpdate().set("$foundation_tutorial_eventRef", this);
		
		PersonAPI commander = fleetAlly.getCommander();
		commander.getName().setFirst(StringHelper.getString("foundation_tutorial", "contactCombatGivenName"));
		commander.getName().setLast(StringHelper.getString("foundation_tutorial", "contactCombatSurname"));
		commander.getName().setGender(FullName.Gender.FEMALE);
		commander.setPortraitSprite("graphics/portraits/portrait_hegemony03.png");
		commander.setPersonality(Personalities.STEADY);
		mission.setContactCombat(commander);
		
		FleetParams fleetParamsEnemy = new FleetParams(null, marketEnemy, Factions.PIRATES, null, FleetTypes.MERC_PRIVATEER, 
				FP_ENEMY - 2, // combat
				0,		 // freighters
				0,		// tankers
				0,		// personnel transports
				0,		// liners
				0,		// civilian
				2,	// utility
				0, 0.35f, 0, 0);	// quality bonus, quality override, officer num mult, officer level bonus
		fleetEnemy = FleetFactoryV2.createFleet(fleetParamsEnemy);
		fleetEnemy.setAIMode(true);
		fleetEnemy.getMemoryWithoutUpdate().set(MemFlags.MEMORY_KEY_PIRATE, true);
		fleetEnemy.getMemoryWithoutUpdate().set("$foundation_tutorial_fleetEnemy", true);
		fleetEnemy.getMemoryWithoutUpdate().set("$foundation_tutorial_eventRef", this);
		fleetEnemy.getMemoryWithoutUpdate().set(MemFlags.MEMORY_KEY_LOW_REP_IMPACT, true);
		fleetEnemy.getCommander().setPersonality(Personalities.STEADY);
		
		/*
		loc.addEntity(fleetAlly);
		fleetAlly.setLocation(posAlly.getLocation().x, posAlly.getLocation().y);
		fleetAlly.addAssignment(FleetAssignment.ORBIT_PASSIVE, posAlly, 99999);
		//fleetAlly.addAssignment(FleetAssignment.ORBIT_AGGRESSIVE, posAlly, 99999);
		fleetAlly.setTransponderOn(false);
		loc.addEntity(fleetEnemy);
		fleetEnemy.setLocation(posEnemy.getLocation().x, posEnemy.getLocation().y);
		fleetEnemy.addAssignment(FleetAssignment.ORBIT_PASSIVE, posEnemy, 99999);
		//fleetEnemy.addAssignment(FleetAssignment.ORBIT_AGGRESSIVE, posEnemy, 99999);
		*/
	}
	
	protected void spawnBountyFleet() {
		MarketAPI bountyMarket = mission.getMarketBounty();
		MarketAPI bountyMarketLoc = mission.getMarketBountyLoc();
		
		FleetParams fleetParams = new FleetParams(null, bountyMarket, Factions.DIKTAT, null, FleetTypes.TRADE_LINER, 
				FP_BOUNTY, // combat
				0,		 // freighters
				0,		// tankers
				0,		// personnel transports
				3,		// liners
				0,		// civilian
				0,	// utility
				0, 0.5f, 0, 0);	// quality bonus, quality override, officer num mult, officer level bonus
		
		fleetBounty = FleetFactoryV2.createFleet(fleetParams);
		fleetBounty.setAIMode(true);
		fleetBounty.getMemoryWithoutUpdate().set("$foundation_tutorial_fleetBounty", true);
		fleetBounty.getMemoryWithoutUpdate().set("$foundation_tutorial_eventRef", this);
		fleetBounty.setName(StringHelper.getString("foundation_tutorial", "bountyFleetName"));
		
		bountyMarketLoc.getStarSystem().addEntity(fleetBounty);
		SectorEntityToken bountyOrbit = bountyMarketLoc.getPrimaryEntity();
		SectorEntityToken bountyHome = bountyMarket.getPrimaryEntity();
		
		mission.setContactBounty(fleetBounty.getCommander());
		
		fleetBounty.setLocation(bountyOrbit.getLocation().x, bountyOrbit.getLocation().y);
		fleetBounty.addAssignment(FleetAssignment.ORBIT_PASSIVE, bountyOrbit, BOUNTY_DAYS);
		fleetBounty.addAssignment(FleetAssignment.GO_TO_LOCATION, bountyHome, 999);
		fleetBounty.addAssignment(FleetAssignment.ORBIT_PASSIVE, bountyHome, 1);
		fleetBounty.addAssignment(FleetAssignment.GO_TO_LOCATION_AND_DESPAWN, bountyHome, 999, new Script() {
			@Override
			public void run() {
				failBounty();
			}
		});
	}
	
	protected void spawnBackupFleet() {
		if (fleetBounty == null || !fleetBounty.isAlive()) return;
		if (fleetBountyBackup != null) return;	// maybe
		MarketAPI bountyMarket = mission.getMarketBounty();
		
		FleetParams fleetParams = new FleetParams(null, bountyMarket, Factions.DIKTAT, null, FleetTypes.TRADE_LINER, 
				FP_BOUNTY_BACKUP, // combat
				0,		 // freighters
				0,		// tankers
				0,		// personnel transports
				0,		// liners
				0,		// civilian
				0,	// utility
				0, 0.5f, 0, 0);	// quality bonus, quality override, officer num mult, officer level bonus
		
		fleetBountyBackup = FleetFactoryV2.createFleet(fleetParams);
		fleetBountyBackup.setAIMode(true);
		
		bountyMarket.getStarSystem().addEntity(fleetBountyBackup);
		SectorEntityToken bountyHome = bountyMarket.getPrimaryEntity();
		
		fleetBounty.setLocation(bountyHome.getLocation().x, bountyHome.getLocation().y);
		fleetBounty.addAssignment(FleetAssignment.ORBIT_PASSIVE,  bountyHome, 1);
		fleetBounty.addAssignment(FleetAssignment.ORBIT_AGGRESSIVE, fleetBounty, 999);
		fleetBounty.addAssignment(FleetAssignment.ORBIT_PASSIVE, bountyHome, 1);
		fleetBounty.addAssignment(FleetAssignment.GO_TO_LOCATION_AND_DESPAWN, bountyHome, 999);
	}
	
	protected void startCombat() {
		if (combatActivated) return;
		
		system.addEntity(fleetAlly);
		fleetAlly.setLocation(posAlly.getLocation().x, posAlly.getLocation().y);
		system.addEntity(fleetEnemy);
		fleetEnemy.setLocation(posEnemy.getLocation().x, posEnemy.getLocation().y);
		
		SectorEntityToken homebase = mission.getMarketStealth().getPrimaryEntity();
		fleetAlly.clearAssignments();
		fleetAlly.addAssignment(FleetAssignment.INTERCEPT, fleetEnemy, 100);
		//fleetAlly.addAssignment(FleetAssignment.GO_TO_LOCATION, asteroids, 2);
		fleetAlly.addAssignment(FleetAssignment.GO_TO_LOCATION, homebase, 100);
		fleetAlly.addAssignment(FleetAssignment.ORBIT_PASSIVE, homebase, 1);
		fleetAlly.addAssignment(FleetAssignment.GO_TO_LOCATION_AND_DESPAWN, homebase, 100);
		fleetAlly.setTransponderOn(true);
		//fleetAlly.getMemoryWithoutUpdate().set("$knowsWhoPlayerIs", true, 5);
		//fleetAlly.getMemoryWithoutUpdate().set(MemFlags.MEMORY_KEY_SAW_PLAYER_WITH_TRANSPONDER_ON, true, 5);

		homebase = mission.getMarketPirate().getPrimaryEntity();
		fleetEnemy.clearAssignments();
		fleetEnemy.addAssignment(FleetAssignment.INTERCEPT, fleetAlly, 100);
		//fleetEnemy.addAssignment(FleetAssignment.ORBIT_AGGRESSIVE, asteroids, 2);
		fleetEnemy.addAssignment(FleetAssignment.GO_TO_LOCATION, homebase, 100);
		fleetEnemy.addAssignment(FleetAssignment.ORBIT_PASSIVE, homebase, 1);
		fleetEnemy.addAssignment(FleetAssignment.GO_TO_LOCATION_AND_DESPAWN, homebase, 100);
		fleetEnemy.setTransponderOn(true);
		
		combatActivated = true;
	}
	
	// TODO
	protected void failBounty() {
		stage = "end_bounty_failure";
		Global.getSector().reportEventStage(this, stage, mission.getMarketBountyLoc().getPrimaryEntity(), 
				MessagePriority.ENSURE_DELIVERY, deliveryFailure);
		endEvent();
	}
	
	// TODO
	protected void startBountyStage() {
		stage = "bounty_start";
		PersonAPI contact = mission.getContactPirate();
		setContactFlags(contact, "pirate");
		mission.getMarket().getCommDirectory().addPerson(contact);
		mission.setImportantPerson(contact);
		
		Global.getSector().reportEventStage(this, stage, mission.getMarketStealth().getPrimaryEntity(), MessagePriority.ENSURE_DELIVERY);
		
	}
	
	public String getStage() {
		return stage;
	}
	
	@Override
	public void advance(float amount) {
		if (!isEventStarted()) return;
		if (isDone()) return;
		
		//updateDaysLeft();
		
		float days = Global.getSector().getClock().convertToDays(amount);
		
		//memory.advance(days);
		elapsedDays += days;
		
		if (stage.equals("combat")) {
			interval.advance(days);
			if (interval.intervalElapsed()) {
				//if (fleetAlly == null) return;
				//if (fleetEnemy == null) return;
				
				//if (fleetAlly.isVisibleToPlayerFleet() || fleetEnemy.isVisibleToPlayerFleet())
				float distance = Misc.getDistance(Global.getSector().getPlayerFleet().getLocation(), asteroids.getLocation());
				if (distance <= 1500) 
				{
					startCombat();
				}
			}
		}
	}

	@Override
	public boolean callEvent(String ruleId, final InteractionDialogAPI dialog, List<Token> params, Map<String, MemoryAPI> memoryMap) {
		String action = params.get(0).getString(memoryMap);
		
		PersonAPI contactStealth = mission.getContactStealth();
		
		switch (action) {
			case "stage_stealth":
				stage = "stealth";
				
				setContactFlags(contactStealth, "stealth");
				mission.getMarketStealth().getCommDirectory().addPerson(contactStealth);
				mission.setImportantPerson(contactStealth);
				
				PersonAPI contactStart = mission.getContactStart();
				//unsetContactFlags(contactStart, "start");
				mission.getMarket().getCommDirectory().removePerson(contactStart);
				
				Global.getSector().reportEventStage(this, stage, mission.getMarketStealth().getPrimaryEntity(), MessagePriority.DELIVER_IMMEDIATELY);
				break;
			case "stage_combat":
				stage = "combat";
				//createCombatFleets();
				primeCombatFleets();
				mission.setImportantPerson(mission.getContactCombat());
				Global.getSector().reportEventStage(this, stage, Global.getSector().getPlayerFleet(), MessagePriority.DELIVER_IMMEDIATELY);
				break;
			case "stage_bounty":
				stage = "bounty";
				spawnBountyFleet();
				mission.setImportantPerson(fleetBounty.getCommander());
				mission.getMarket().getCommDirectory().removePerson(mission.getContactPirate());
				Global.getSector().reportEventStage(this, stage, Global.getSector().getPlayerFleet(), MessagePriority.DELIVER_IMMEDIATELY);
				break;
			case "end":
				stage = "end";
				Global.getSector().reportEventStage(this, stage, mission.getMarketStealth().getPrimaryEntity(), MessagePriority.DELIVER_IMMEDIATELY);
				endEvent();
				break;
			case "quit":
				stage = "quit";
				Global.getSector().reportEventStage(this, stage, Global.getSector().getPlayerFleet(), MessagePriority.DELIVER_IMMEDIATELY);
				endEvent();
				break;
			case "stealth_complete":
				launchRecruitFleet(mission.getMarketStealth());
				
				//unsetContactFlags(contactStealth, "stealth");
				mission.getMarketStealth().getCommDirectory().removePerson(contactStealth);
				//mission.getMarketStealth().removePerson(contactStealth);
				break;
			case "combat_start":
				startCombat();
				break;
			//case "combat_complete":
			//	break;
				
			// hee hee
			case "getLadOrLass":
				String term = StringHelper.getString("foundation_tutorial", "lad");
				if (Global.getSector().getPlayerPerson().isFemale()) 
					term = StringHelper.getString("foundation_tutorial", "lass");
				memoryMap.get(MemKeys.PLAYER).set("$ladOrLass", term, 0);
				break;
			case "getAsteroidsName":
				memoryMap.get(MemKeys.LOCAL).set("$foundation_tutorial_asteroids", StringHelper.getString("foundation_tutorial", "asteroidsName"), 0);
				break;
			case "getStartContactSurname":
				memoryMap.get(MemKeys.LOCAL).set("$foundation_tutorial_startContactSurname", mission.getContactStart().getName().getLast(), 0);
				break;
			case "getStealthContactSurname":
				memoryMap.get(MemKeys.LOCAL).set("$foundation_tutorial_stealthContactSurname", mission.getContactStealth().getName().getLast(), 0);
				break;
			case "raiseRep":
				Global.getSector().adjustPlayerReputation(
									new RepActionEnvelope(RepActions.CUSTOM, mission.getRepChange(0.03f),
											  null, dialog.getTextPanel(), true),
											  dialog.getInteractionTarget().getActivePerson());
				break;
			case "lowerRep":
				Global.getSector().adjustPlayerReputation(
									new RepActionEnvelope(RepActions.CUSTOM, mission.getRepChange(-0.03f),
											null, dialog.getTextPanel(), true), 
											dialog.getInteractionTarget().getActivePerson());
				break;
			case "logEventEnded":
				Global.getSector().getCampaignUI().addMessage(ended + "");
		}
		return true;
	}	

	@Deprecated
	protected void updateDaysLeft() {
		
	}
	
	protected void processBattleCombatStage(CampaignFleetAPI primaryWinner, BattleAPI battle) {
		if (!battle.isInvolved(fleetAlly) && !battle.isInvolved(fleetEnemy))
			return;	// nothing to do with us
		//Global.getSector().getCampaignUI().addMessage(battle.isDone() + "");

		List<CampaignFleetAPI> losers = battle.getSnapshotBothSides();
		losers.removeAll(battle.getSnapshotSideFor(primaryWinner));
		CampaignFleetAPI playerFleet = Global.getSector().getPlayerFleet();

		mission.setContactCombat(fleetAlly.getCommander());
		mission.setImportantPerson(mission.contactCombat);

		//Global.getSector().getCampaignUI().addMessage(losers.size() + ", " + primaryWinner.getName());

		// teamkill
		if (battle.isPlayerInvolved() && battle.isInvolved(fleetAlly) && !battle.isOnPlayerSide(fleetAlly)) {
			stage = "end_combat_teamkill";
			Global.getSector().reportEventStage(this, stage, playerFleet, MessagePriority.ENSURE_DELIVERY, deliveryFailure);
			fleetAlly.getMemoryWithoutUpdate().set("$tutorialBattleOver", true);
			endEvent();
			return;
		}

		// enemy lost
		if (losers.contains(fleetEnemy)) {
			// enemy attacked by unrelated third party
			if (!battle.isInvolved(fleetAlly) && !battle.isInvolved(playerFleet))
			{
				// enemy fleet knocked out
				if (!fleetEnemy.isAlive() || fleetEnemy.getFleetPoints() > FP_ENEMY / 2f)
					return;

				stage = "combat_cancelled_enemy";
				Global.getSector().reportEventStage(this, stage, fleetAlly, MessagePriority.ENSURE_DELIVERY);
				fleetAlly.getMemoryWithoutUpdate().set("$tutorialBattleOver", true);
				startBountyStage();
			}
			// player or ally fleet defeated enemy
			else {
				// player didn't help
				if (!battle.isPlayerInvolved() || battle.getPlayerInvolvementFraction() <= 0) {
					stage = "end_combat_ignored";
					Global.getSector().reportEventStage(this, stage, fleetAlly, MessagePriority.ENSURE_DELIVERY, deliveryFailure);
					fleetAlly.getMemoryWithoutUpdate().set(MemFlags.MEMORY_KEY_IGNORE_PLAYER_COMMS, true, 2);
					fleetAlly.getMemoryWithoutUpdate().set("$tutorialBattleOver", true);
					endEvent();
				}
				// player did help
				else {
					stage = "combat_win";
					Global.getSector().reportEventStage(this, stage, fleetAlly, MessagePriority.ENSURE_DELIVERY, deliverySuccess);
					fleetAlly.getMemoryWithoutUpdate().set("$tutorialBattleOver", true);
					fleetAlly.getMemoryWithoutUpdate().set("$tutorialBattleWon", true);
					startBountyStage();
				}
			}
		}
		else if (losers.contains(fleetAlly)) {
			// ally attacked by unrelated third party
			if (!battle.isInvolved(fleetEnemy) && !battle.isInvolved(playerFleet))
			{
				// ally fleet knocked out
				if (!fleetAlly.isAlive() || fleetAlly.getFleetPoints() > FP_ALLY / 2f)
					return;

				stage = "combat_cancelled_ally";
				Global.getSector().reportEventStage(this, stage, mission.getMarketStealth().getPrimaryEntity(), MessagePriority.ENSURE_DELIVERY);
				fleetAlly.getMemoryWithoutUpdate().set("$tutorialBattleOver", true);
				startBountyStage();
			}
			else {
				// player didn't help
				if (!battle.isPlayerInvolved() || battle.getPlayerInvolvementFraction() <= 0) {
					stage = "combat_ignored";
					Global.getSector().reportEventStage(this, stage, fleetAlly, MessagePriority.ENSURE_DELIVERY, deliveryFailure);
					fleetAlly.getMemoryWithoutUpdate().set(MemFlags.MEMORY_KEY_IGNORE_PLAYER_COMMS, true, 2);
					fleetAlly.getMemoryWithoutUpdate().set("$tutorialBattleOver", true);
					endEvent();
				}
				// player did help (but still lost q__q)
				else {
					stage = "combat_lose";
					Global.getSector().reportEventStage(this, stage, fleetAlly, MessagePriority.ENSURE_DELIVERY);
					fleetAlly.getMemoryWithoutUpdate().set("$tutorialBattleOver", true);
					startBountyStage();
				}
			}
		}
	}
	
	// taken from PersonBountyEvent
	protected void processBattleBountyStage(CampaignFleetAPI primaryWinner, BattleAPI battle) 
	{
		if (!battle.isInvolved(fleetBounty))
			return;	// nothing to do with us
		
		if (battle.isInvolved(fleetBounty) && !battle.isPlayerInvolved()) {
			if (fleetBounty.getFlagship() == null || fleetBounty.getFlagship().getCaptain() != mission.getContactBounty()) {
				fleetBounty.setCommander(fleetBounty.getFaction().createRandomPerson());
				stage = "end_bounty_other";
				Global.getSector().reportEventStage(this, stage, mission.getMarketBountyLoc().getPrimaryEntity(), MessagePriority.ENSURE_DELIVERY);
				endEvent();
				return;
			}
		}
		
		if (!battle.isPlayerInvolved() || battle.onPlayerSide(fleetBounty)) {
			return;
		}
		
		 // didn't destroy the original flagship
		if (fleetBounty.getFlagship() != null && fleetBounty.getFlagship().getCaptain() == mission.getContactBounty()) return;
		
		bountyPayment = (int) (BOUNTY_AMOUNT * battle.getPlayerInvolvementFraction());
		if (bountyPayment <= 0) return;
		
		String reportId = "end_bounty_success";
		if (battle.getPlayerInvolvementFraction() < 1) {
			reportId = "end_bounty_success_share";
		}
		//log.info(String.format("Paying bounty of %d from faction [%s]", (int) bountyPayment, faction.getDisplayName()));
		Global.getSector().reportEventStage(this, reportId, mission.getMarketBountyLoc().getPrimaryEntity(),
									MessagePriority.ENSURE_DELIVERY, new BaseOnMessageDeliveryScript() {
			public void beforeDelivery(CommMessageAPI message) {
				CampaignFleetAPI playerFleet = Global.getSector().getPlayerFleet();
				playerFleet.getCargo().getCredits().add(bountyPayment);
				Global.getSector().adjustPlayerReputation(
						new RepActionEnvelope(RepActions.PERSON_BOUNTY_REWARD, null, message, true), 
						mission.getContactPirate().getFaction().getId());
			}
		});
	}
	
	// use this instead of reportBattleFinished for NPC battles, due to a bug in when the battle ends
	// http://fractalsoftworks.com/forum/index.php?topic=5061.msg187401#msg187401
	// note: isDone() is never true in a player battle
	@Override
	public void reportBattleOccurred(CampaignFleetAPI primaryWinner, BattleAPI battle) {
		if (isDone()) return;
		//if (!battle.isDone() && !battle.isPlayerInvolved()) return;
		if (stage.equals("combat")) {
			if (!battle.isDone() && !battle.isPlayerInvolved()) return;
			processBattleCombatStage(primaryWinner, battle);
		}
		else if (stage.equals("bounty")) {
			processBattleBountyStage(primaryWinner, battle);
		}
	}
	
	@Override
	public Map<String, String> getTokenReplacements() {
		updateDaysLeft();
		
		Map<String, String> map = super.getTokenReplacements();
		map.put("$sender", "Mission Board");
		PersonAPI contact = null;
		PersonAPI target = null;
		switch (stage) {
			case "start":
				contact = mission.getContactStart();
				map.put("$market", mission.getMarket().getName());
				break;
			case "stealth":
				contact = mission.getContactStealth();
				map.put("$market", mission.getMarketStealth().getName());
				map.put("$location", mission.getMarketStealth().getContainingLocation().getName());
				break;
			case "combat":
				contact = mission.getContactCombat();
				map.put("$asteroids", StringHelper.getString("foundation_tutorial", "asteroidsName"));
				map.put("$planet", barad.getName());
				map.put("$location", mission.getMarketStealth().getStarSystem().getBaseName());
				break;
			case "end_combat_ignored":
			case "combat_win":
			case "combat_lose":
			case "combat_cancelled_enemy":
			case "combat_cancelled_ally":
				contact = mission.getContactCombat();
				map.put("$asteroids", StringHelper.getString("foundation_tutorial", "asteroidsName"));
				break;
			case "bounty_start":
				contact = mission.getContactPirate();
				map.put("$market", mission.getMarket().getName());
				break;
			case "bounty":
				// todo
				contact = mission.getContactPirate();
				target = mission.getContactBounty();
				map.put("$market", mission.getMarketBountyLoc().getName());
				map.put("$location", mission.getMarketBountyLoc().getStarSystem().getName());
				map.put("$bountyAmount", Misc.getDGSCredits(BOUNTY_AMOUNT));
				map.put("$timeLimit", (int)BOUNTY_DAYS + "");
				break;
			case "end_bounty_success":
			case "end_bounty_success_share":
				map.put("$bountyAmount", Misc.getDGSCredits(bountyPayment));
				contact = mission.getContactPirate();
				target = mission.getContactBounty();
				break;
			case "end_bounty_failure":
			case "end_bounty_other":
				contact = mission.getContactPirate();
				target = mission.getContactBounty();
				break;
			case "end":
		}
		if (contact != null) {
			addPersonTokens(map, "contact", contact);
			String desc = contact.getRank();
			map.put("$contactDesc", desc);
		}
		if (target != null) {
			addPersonTokens(map, "target", target);
			String desc = target.getRank();
			map.put("$targetDesc", desc);
		}
		return map;
	}

	@Override
	public String[] getHighlights(String stageId) {
		List<String> result = new ArrayList<>();
		
		switch (stageId) {
			case "start":
			case "stealth":
			case "bounty_start":
				addTokensToList(result, "$market");
				break;
			case "combat":
				addTokensToList(result, "$asteroids");
				addTokensToList(result, "$location");
				break;
			case "bounty":
				addTokensToList(result, "$bountyAmount");
				addTokensToList(result, "$market");
				addTokensToList(result, "$location");
				addTokensToList(result, "$timeLimit");
				break;
			case "end_bounty_success":
			case "end_bounty_success_share":
				addTokensToList(result, "$bountyAmount");
		}
		
		return result.toArray(new String[0]);
	}
	
	@Override
	public Color[] getHighlightColors(String stageId) {
		return super.getHighlightColors(stageId);
	}

	@Override
	public boolean isDone() {
		return ended;
	}

	// TODO
	@Override
	public String getEventName() {
		return mission.getName();
	}

	@Override
	public CampaignEventCategory getEventCategory() {
		return CampaignEventCategory.MISSION;
	}

	@Override
	public String getEventIcon() {
		return Global.getSector().getFaction(mission.getFactionId()).getCrest();
	}

	@Override
	public String getCurrentImage() {
		PersonAPI contact = null;
		switch (stage) {
			case "start":
				contact = mission.getContactStart();
				break;
			case "stealth":
				contact = mission.getContactStealth();
				break;
			case "combat":
			case "end_combat_ignored":
			case "combat_win":
			case "combat_lose":
			case "combat_cancelled_enemy":
			case "combat_cancelled_ally":
				contact = mission.getContactCombat();
				break;
			case "bounty_start":
				contact = mission.getContactPirate();
				break;
			case "bounty":
			case "end_bounty_success":
			case "end_bounty_success_share":
			case "end_bounty_failure":
			case "end_bounty_other":
				contact = mission.getContactBounty();
				break;
		}
		if (contact != null) return contact.getPortraitSprite();
		return Global.getSector().getFaction(mission.getFactionId()).getLogo();
	}
}