package org.histidine.foundation.campaign.missions;

import com.fs.starfarer.api.EveryFrameScript;
import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.CampaignUIAPI;
import com.fs.starfarer.api.campaign.FactionAPI;
import com.fs.starfarer.api.campaign.SectorAPI;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.characters.FullName;
import com.fs.starfarer.api.characters.ImportantPeopleAPI;
import com.fs.starfarer.api.characters.PersonAPI;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.impl.campaign.ids.MemFlags;
import com.fs.starfarer.api.impl.campaign.ids.Ranks;
import exerelin.campaign.PlayerFactionStore;
import org.apache.log4j.Logger;
import org.histidine.foundation.campaign.missions.TutorialMission.TutorialMissionArgs;
import org.histidine.foundation.scripts.FoundationModPlugin;
import org.histidine.foundation.scripts.StringHelper;
import org.histidine.foundation.scripts.world.FoundationSectorGen;

@Deprecated
public class TutorialMissionCreator implements EveryFrameScript {
	
	public static Logger log = Global.getLogger(TutorialMissionCreator.class);
			
	protected boolean done = false;
	protected int delay = 0;
	
	public void create(SectorAPI sector)
	{
		if (FoundationModPlugin.HAVE_NEXERELIN) {
			String factionId = PlayerFactionStore.getPlayerFactionId();
			if (!factionId.equals(Factions.HEGEMONY) && !factionId.equals("player_npc"))
				return;
		}
		
		if (FoundationModPlugin.HAVE_RAGS_2_RICHES)
			return;
		
		if (Global.getSector().getPlayerFleet().getCargo().getCredits().get() > 50000)
			return;
		
		if (Global.getSector().getPlayerFleet().getFleetSizeCount() > 4)
			return;
		
		SectorEntityToken jangala = Global.getSector().getEntityById("jangala");
		if (jangala == null) return;
		
		if (!Global.getSector().getPlayerFaction().isHostileTo(Factions.PIRATES)) 
			return;
		
		// make characters
		ImportantPeopleAPI ip = sector.getImportantPeople();
		FactionAPI independent = sector.getFaction(Factions.INDEPENDENT);
		
		MarketAPI jangalaMarket = jangala.getMarket();
		MarketAPI lawsonsMarket = Global.getSector().getEntityById(FoundationSectorGen.FOUNDATION_HQ_ID).getMarket();
		
		PersonAPI contactStart = independent.createRandomPerson(FullName.Gender.FEMALE);
		contactStart.getName().setFirst(StringHelper.getString("foundation_tutorial", "contactStartGivenName"));
		contactStart.getName().setLast(StringHelper.getString("foundation_tutorial", "contactStartSurname"));
		contactStart.setRankId(Ranks.CITIZEN);
		contactStart.setPostId("foundationAgent");
		contactStart.setPortraitSprite("graphics/portraits/portrait_corporate04.png");
		jangalaMarket.addPerson(contactStart);
		//ip.addPerson(contactStart);
		//ip.getData(contactStart).getLocation().setMarket(jangalaMarket);

		PersonAPI contactStealth = independent.createRandomPerson(FullName.Gender.MALE);
		contactStealth.getName().setFirst(StringHelper.getString("foundation_tutorial", "contactStealthGivenName"));
		contactStealth.getName().setLast(StringHelper.getString("foundation_tutorial", "contactStealthSurname"));
		contactStealth.setRankId(Ranks.CITIZEN);
		contactStealth.setPostId("foundationAgent");
		contactStealth.setPortraitSprite("graphics/portraits/portrait18.png");
		contactStealth.getMemoryWithoutUpdate().set(MemFlags.MEMORY_KEY_REQUIRES_DISCRETION, true);
		lawsonsMarket.addPerson(contactStealth);
		//ip.addPerson(contactStealth);
		//ip.getData(contactStealth).getLocation().setMarket(jangalaMarket);
		
		PersonAPI contactPirate = Global.getSector().getFaction(Factions.PIRATES).createRandomPerson(FullName.Gender.MALE);
		contactPirate.getName().setFirst(StringHelper.getString("foundation_tutorial", "contactPirateGivenName"));
		contactPirate.getName().setLast(StringHelper.getString("foundation_tutorial", "contactPirateSurname"));
		contactPirate.setRankId(Ranks.CITIZEN);
		contactPirate.setPostId(Ranks.POST_GANGSTER);
		contactPirate.setPortraitSprite("graphics/portraits/portrait_mercenary04.png");
		contactPirate.getMemoryWithoutUpdate().set(MemFlags.MEMORY_KEY_REQUIRES_DISCRETION, true);
		jangalaMarket.addPerson(contactPirate);
		
		TutorialMissionArgs args = new TutorialMissionArgs();
		args.contactStart = contactStart;
		args.contactStealth = contactStealth;
		args.contactPirate = contactPirate;
		args.market = jangalaMarket;
		args.marketStealth = lawsonsMarket;
		args.marketPirate = Global.getSector().getEconomy().getMarket("corvus_IIIa");
		args.marketBounty = Global.getSector().getEconomy().getMarket("sindria");
		args.marketBountyLoc = Global.getSector().getEconomy().getMarket("nortia");
		args.recruit_shipVariant = "wolf_Starting";
		
		TutorialMission mission = new TutorialMission(args);
		mission.playerAccept(jangala);
	}

	@Override
	public boolean isDone() {
		return done;
	}

	@Override
	public boolean runWhilePaused() {
		return false;
	}

	@Override
	public void advance(float amount)
	{
		CampaignUIAPI ui = Global.getSector().getCampaignUI();
		if (Global.getSector().isInNewGameAdvance() || ui.isShowingDialog())
		{
			return;
		}
		delay++;
		if (delay >= 3) {
			create(Global.getSector());
			done = true;
		}
		
	}
}
