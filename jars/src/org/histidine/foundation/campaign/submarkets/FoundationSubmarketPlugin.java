package org.histidine.foundation.campaign.submarkets;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.ModManagerAPI;
import com.fs.starfarer.api.campaign.CampaignUIAPI.CoreUITradeMode;
import com.fs.starfarer.api.campaign.CargoAPI;
import com.fs.starfarer.api.campaign.CargoStackAPI;
import com.fs.starfarer.api.campaign.CoreUIAPI;
import com.fs.starfarer.api.campaign.econ.SubmarketAPI;
import com.fs.starfarer.api.fleet.FleetMemberAPI;
import com.fs.starfarer.api.fleet.FleetMemberType;
import com.fs.starfarer.api.impl.campaign.submarkets.BaseSubmarketPlugin;
import com.fs.starfarer.api.loading.WeaponSpecAPI;
import com.fs.starfarer.api.util.Highlights;
import com.fs.starfarer.api.util.Misc;
import com.fs.starfarer.api.util.WeightedRandomPicker;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import org.histidine.foundation.scripts.StringHelper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class FoundationSubmarketPlugin extends BaseSubmarketPlugin {
	
	public static final String STRING_HELPER_KEY = "foundation";
	public static final String ILLEGAL_TRANSFER_MESSAGE = StringHelper.getString(STRING_HELPER_KEY, "storeNoSale");
	public static final int MAX_LEVEL = 15;	// TODO config
	public static final int NUM_WEAPONS = 15;
	public static final int NUM_WINGS = 4;
	public static final int NUM_SHIPS = 12;
	public static final String AVAILABLE_SHIPS_CSV = "data/config/foundation/foundationShips.csv";
	public static final List<String> AVAILABLE_SHIPS = new ArrayList<>();
	
	public static Logger log = Global.getLogger(FoundationSubmarketPlugin.class);
	
	static {
		try {
			// hax for compatibility with both Starsector+ and Ship/Weapon Pack
			ModManagerAPI modManager = Global.getSettings().getModManager();
			
			JSONArray ships = Global.getSettings().getMergedSpreadsheetDataForMod("id", AVAILABLE_SHIPS_CSV, "SteinerFoundation");
			for(int x = 0; x < ships.length(); x++)
			{
				JSONObject row = ships.getJSONObject(x);
				String id = row.getString("id");
				String requiredMod = row.optString("requiredMod");
				//log.info("Required mod: " + requiredMod);
				if (!requiredMod.isEmpty() && !modManager.isModEnabled(requiredMod))
					continue;
				AVAILABLE_SHIPS.add(id);
			}
		}
		catch(IOException | JSONException ex)
		{
			log.error(ex);
		}
	}

	@Override
	public void updateCargoPrePlayerInteraction() 
	{
		if (!okToUpdateCargo()) return;
		sinceLastCargoUpdate = 0f;
		
		CargoAPI cargo = getCargo();
				
		pruneWeapons(0.75f);
		addWeaponsBasedOnMarketSize(NUM_WEAPONS, 0, 2, null);
		addRandomWeapons(NUM_WEAPONS/2, 3);
		//addRandomFoundationWeapons(NUM_WEAPONS, 0, 2);

		pruneShips(0.5f);
		addShips();
		addWings();
		cargo.sort();
	}
	
	protected void addShips() {		
		WeightedRandomPicker<String> picker = new WeightedRandomPicker<>();
		for (String id : AVAILABLE_SHIPS) 
		{
			if (id.endsWith("_wing")) continue;
			picker.add(id);
		}
		
		for (int i=getCargo().getMothballedShips().getMembersListCopy().size(); i < NUM_SHIPS; i++)
		{
			if (picker.isEmpty()) return;
			String id = picker.pickAndRemove();
			id += "_Hull";
			try { 
				FleetMemberAPI member = Global.getFactory().createFleetMember(FleetMemberType.SHIP, id);
				member.getRepairTracker().setMothballed(true);
				getCargo().getMothballedShips().addFleetMember(member);
			} catch (RuntimeException rex) {
				// ship doesn't exist; meh
			}
		}
	}
	
	protected void addWings() {
		WeightedRandomPicker<String> picker = new WeightedRandomPicker<>();
		for (String id : AVAILABLE_SHIPS) 
		{
			if (!id.endsWith("_wing")) continue;
			picker.add(id);
		}
		
		for (int i=getCargo().getFighters().size(); i < NUM_WINGS; i++)
		{
			if (picker.isEmpty()) return;
			String id = picker.pickAndRemove();
			try { 
				getCargo().addItems(CargoAPI.CargoItemType.FIGHTER_CHIP, id, 1);
			} catch (RuntimeException rex) {
				// wing doesn't exist; meh
			}
		}
	}

	protected void addRandomFoundationWeapons(int max, int minTier, int maxTier) {
		CargoAPI cargo = getCargo();
		List<String> weaponIds = Global.getSector().getAllWeaponIds();
		
		WeightedRandomPicker<String> picker = new WeightedRandomPicker<>();
		
		for (String id : weaponIds) {
			if (id.startsWith("tem_")) continue;
			WeaponSpecAPI spec = Global.getSettings().getWeaponSpec(id);
			int tier = spec.getTier();
			if (tier >= minTier && tier <= maxTier) {
				//float weaponQuality = 0.33f * (float) spec.getTier();
				//float qualityDiff = Math.abs(qualityFactor - weaponQuality);
				float qualityDiff = 0f;
				float f = Math.max(0, 1f - qualityDiff);
				float weight = Math.max(0.05f, f * f);
				picker.add(spec.getWeaponId(), weight);
			}
		}
		if (!picker.isEmpty()) {
			for (int i = 0; i < max; i++) {
				String weaponId = picker.pick();
				int quantity = 2;
				WeaponSpecAPI spec = Global.getSettings().getWeaponSpec(weaponId);
				switch (spec.getSize()) {
					case LARGE:
						quantity = 1;
						break;
					case MEDIUM:
						quantity = 2;
						break;
					case SMALL:
						quantity = 3;
						break;
				}
				cargo.addWeapons(weaponId, quantity);
			}	
		}
	}
	
	@Override
	public String getName() {
		return StringHelper.getString(STRING_HELPER_KEY, "storeName");
	}
	
	@Override
	public float getTariff() {
			return -0.2f;	// TODO config
	}
	
	@Override
	public boolean isIllegalOnSubmarket(CargoStackAPI stack, TransferAction action) {
		if (action == TransferAction.PLAYER_SELL) return true;
		return false;
	}


	@Override
	public boolean isIllegalOnSubmarket(String commodityId, TransferAction action) {
		if (action == TransferAction.PLAYER_SELL) return true;
		return false;
	}
	
	@Override
	public boolean isIllegalOnSubmarket(FleetMemberAPI member, TransferAction action) {
		if (action == TransferAction.PLAYER_SELL) return true;
		return false;
	}
	
	@Override
	public String getIllegalTransferText(FleetMemberAPI member, TransferAction action) {
		return ILLEGAL_TRANSFER_MESSAGE;
	}
	
	@Override
	public String getIllegalTransferText(CargoStackAPI stack, TransferAction action) {
		return ILLEGAL_TRANSFER_MESSAGE;
	}
	
	@Override
	public boolean isEnabled(CoreUIAPI ui) {
		//if (mode == CoreUITradeMode.OPEN) return false;
		if (ui.getTradeMode() == CoreUITradeMode.SNEAK) return false;
		
		int level = Global.getSector().getPlayerPerson().getStats().getLevel();
		return level <= MAX_LEVEL;
	}
	
	@Override
	public String getTooltipAppendix(CoreUIAPI ui) {
		int level = Global.getSector().getPlayerPerson().getStats().getLevel();
		if (level > MAX_LEVEL) {
			return StringHelper.getStringAndSubstituteToken(STRING_HELPER_KEY, "storeMaxLevelReq", "$maxLevel", MAX_LEVEL + "");
		}
		if (ui.getTradeMode() == CoreUITradeMode.SNEAK) {
			return "Requires: proper docking authorization";
		}
		return null;
	}
	
	@Override
	public Highlights getTooltipAppendixHighlights(CoreUIAPI ui) {
		String appendix = getTooltipAppendix(ui);
		if (appendix == null) return null;
		
		Highlights h = new Highlights();
		h.setText(appendix);
		h.setColors(Misc.getNegativeHighlightColor());
		return h;
	}
}
