package org.histidine.foundation.scripts.world;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.LocationAPI;
import com.fs.starfarer.api.campaign.PlanetAPI;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.campaign.econ.EconomyAPI;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.impl.campaign.ids.Terrain;
import com.fs.starfarer.api.impl.campaign.terrain.MagneticFieldTerrainPlugin;
import java.awt.Color;
import java.util.ArrayList;


public class SystemGenUtils 
{
    public static float getOrbitalPeriod(float primaryRadius, float orbitRadius, float density)
    {
        primaryRadius *= 0.01;
        orbitRadius *= 1/62.5;

        float mass = (float)Math.floor(4f / 3f * Math.PI * Math.pow(primaryRadius, 3));
        mass *= density;
        float radiusCubed = (float)Math.pow(orbitRadius, 3);
        float period = (float)(2 * Math.PI * Math.sqrt(radiusCubed/mass) * 2);
        return period;
    }
    
    public static float getOrbitalPeriod(SectorEntityToken primary, float orbitRadius)
    {
        return getOrbitalPeriod(primary.getRadius(), orbitRadius, getDensity(primary));
    }

    public static float getDensity(SectorEntityToken primary)
    {
        if (primary instanceof PlanetAPI)
        {
                PlanetAPI planet = (PlanetAPI)primary;
                if (planet.getTypeId().equals("star_dark")) return 8;
                else if (planet.isGasGiant()) return 0.5f;
                else if (planet.isStar()) return 0.5f;
        }
        return 2;
    }
    
    public static void addMagneticField(SectorEntityToken entity)
    {
        LocationAPI loc = entity.getContainingLocation();
        float radius = entity.getRadius();
        float effectRadius = 200;
        if (entity instanceof PlanetAPI)
        {
            PlanetAPI planet = (PlanetAPI)entity;
            if (planet.isStar()) effectRadius = 800f;
        }
        SectorEntityToken field = loc.addTerrain(Terrain.MAGNETIC_FIELD,
            new MagneticFieldTerrainPlugin.MagneticFieldParams(radius + effectRadius, // terrain effect band width 
                    (radius + effectRadius)/2f, // terrain effect middle radius
                    entity, // entity that it's around
                    radius + effectRadius/4, // visual band start
                    radius + effectRadius/4 + effectRadius, // visual band end
                    new Color(50, 20, 100, 40), // base color
                    1f, // probability to spawn aurora sequence, checked once/day when no aurora in progress
                    new Color(50, 20, 110, 130),
                    new Color(150, 30, 120, 150), 
                    new Color(200, 50, 130, 190),
                    new Color(250, 70, 150, 240),
                    new Color(200, 80, 130, 255),
                    new Color(75, 0, 160), 
                    new Color(127, 0, 255)
                    ));
            field.setCircularOrbit(entity, 0, 0, 100);
    }
    
    // by Debido
    // see http://fractalsoftworks.com/forum/index.php?topic=8581.0
    public static void addMarketplace(String factionID, SectorEntityToken primaryEntity, ArrayList<SectorEntityToken> connectedEntities, String name, 
            int size, ArrayList<String> marketConditions, ArrayList<String> submarkets, float tariff) {
        
        EconomyAPI globalEconomy = Global.getSector().getEconomy();
        String planetID = primaryEntity.getId();

        //generate the market ID
        String marketID = planetID;	//+ "_market";	// market ID must match entity ID for now, so missions can be posted in 0.7

        //generate the market
        MarketAPI newMarket = Global.getFactory().createMarket(marketID, name, size);

        //set the faction associated with the market
        newMarket.setFactionId(factionID);

        //set the primary entity related to the market
        newMarket.setPrimaryEntity(primaryEntity);

        //set the base smuggling value (starting value)
        newMarket.setBaseSmugglingStabilityValue(0);

        //set the starting tarrif, could also make this value an input
        newMarket.getTariff().modifyFlat("generator", tariff);

        //add each sub-market types to the mark
        if (null != submarkets) {
            for (String market : submarkets) {
                newMarket.addSubmarket(market);
            }
        }

        //add each market conditions
        for (String condition : marketConditions) {
            newMarket.addCondition(condition);
        }

        //add all connected entities to this marketplace, moons/stations etc.
        if (null != connectedEntities) {
            for (SectorEntityToken entity : connectedEntities) {
                newMarket.getConnectedEntities().add(entity);
            }
        }

        //add the market to the global market place
        globalEconomy.addMarket(newMarket);

        //get the primary entity and associate it back to the market that we've created
        primaryEntity.setMarket(newMarket);

        //to prevent certain issues, make sure the faction is also set for the primary entity
        primaryEntity.setFaction(factionID);

        //get all associated entities and associate it back to the market we've created
        if (null != connectedEntities) {
            for (SectorEntityToken entity : connectedEntities) {
                entity.setMarket(newMarket);
                entity.setFaction(factionID);
            }
        }
    }
}