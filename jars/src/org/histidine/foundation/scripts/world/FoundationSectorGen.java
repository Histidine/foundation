package org.histidine.foundation.scripts.world;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.CampaignTerrainAPI;
import com.fs.starfarer.api.campaign.PlanetAPI;
import com.fs.starfarer.api.campaign.SectorAPI;
import com.fs.starfarer.api.campaign.SectorEntityToken;
import com.fs.starfarer.api.campaign.StarSystemAPI;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.characters.ImportantPeopleAPI;
import com.fs.starfarer.api.characters.PersonAPI;
import com.fs.starfarer.api.impl.campaign.ids.Conditions;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.impl.campaign.ids.Ranks;
import com.fs.starfarer.api.impl.campaign.ids.Submarkets;
import com.fs.starfarer.api.impl.campaign.ids.Tags;
import com.fs.starfarer.api.impl.campaign.ids.Terrain;
import com.fs.starfarer.api.util.Misc;
import com.fs.starfarer.api.util.WeightedRandomPicker;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.histidine.foundation.scripts.StringHelper;

public class FoundationSectorGen {
	public static final String FOUNDATION_HQ_ID = "foundation_lawson";
	public static final String FOUNDATION_HQ_NAME = StringHelper.getString("foundation", "stationName");
	public static final Set<String> ALLOWED_STATION_TERRAIN = new HashSet<>(Arrays.asList(new String[] {
		Terrain.ASTEROID_BELT, Terrain.ASTEROID_FIELD
	}));
	public static final float MIN_DIST_FROM_EXISTING_STATION_SQ = 1500f * 1500f;
	
	public static boolean isNearExistingMarket(SectorEntityToken planet)
	{
		List<SectorEntityToken> markets = planet.getContainingLocation().getEntitiesWithTag(Tags.STATION);
		markets.addAll(planet.getContainingLocation().getPlanets());
		for (SectorEntityToken station : markets)
		{
			if (station.getMarket() == null || station.getMarket().isPlanetConditionMarketOnly())
				continue;
			
			if (Misc.getDistanceSq(station.getLocation(), planet.getLocation()) < MIN_DIST_FROM_EXISTING_STATION_SQ)
			{
				return true;
			}
		}
		return false;
	}
	
	public static SectorEntityToken getFoundationBase(SectorAPI sector)
	{
		MarketAPI market = sector.getEconomy().getMarket(FOUNDATION_HQ_ID);
		if (market != null) return market.getPrimaryEntity();
		return null;
	}
	
	/**
	 * Create the Lawson's Point entity and market.
	 * This is done before the economy load phase, so that phase can do its usual thing.
	 * @param sector
	 * @return
	 */
	public SectorEntityToken createFoundationBasePt1(SectorAPI sector)
	{
		Global.getLogger(FoundationSectorGen.class).info("Running sector generation part 1");
		SectorEntityToken foundationBase = sector.getHyperspace().addCustomEntity(FOUNDATION_HQ_ID, FOUNDATION_HQ_NAME, "station_side00", Factions.INDEPENDENT);
		foundationBase.getLocation().set(0, 0);
		foundationBase.addTag("uninvadable");
		
		SystemGenUtils.addMarketplace(Factions.INDEPENDENT,  
				foundationBase,
				new ArrayList<>(Arrays.asList((SectorEntityToken) foundationBase)),  
				FOUNDATION_HQ_NAME,
				4,  
				new ArrayList<>(Arrays.asList(Conditions.POPULATION_4, Conditions.ORE_COMPLEX, Conditions.LIGHT_INDUSTRIAL_COMPLEX,
						Conditions.ORBITAL_STATION, Conditions.STEALTH_MINEFIELDS, Conditions.VOLATILES_DEPOT)),  
				new ArrayList<>(Arrays.asList(Submarkets.SUBMARKET_STORAGE, Submarkets.SUBMARKET_BLACK, Submarkets.SUBMARKET_OPEN)),  
				0.25f
		);
		MarketAPI market = foundationBase.getMarket();
		market.setSurveyLevel(MarketAPI.SurveyLevel.FULL);
		market.addSubmarket("foundation_submarket");
		
		return foundationBase;
	}
	
	/**
	 * Places the Foundation base in an appropriate location.
	 * This can't be done before economy load, since in Nexerelin's random sector markets don't exist yet,
	 * so we won't know where to place the station.
	 * @param sector
	 */
	public static void createFoundationBasePt2(SectorAPI sector)
	{
		Global.getLogger(FoundationSectorGen.class).info("Running sector generation part 2");
		SectorEntityToken foundationBase = getFoundationBase(sector);
		SectorEntityToken corvus = sector.getEntityById("corvus");
		SectorEntityToken toOrbit = null;
		float orbitRadius = 5650;	// Nemo's Belt orbit distance
		float orbitPeriod;
		
		// find a suitable planet to orbit
		if (corvus != null)
		{
			toOrbit = corvus;
			orbitPeriod = 300;	// hardcode to match belt speed
		}
		else {
			WeightedRandomPicker<SectorEntityToken> picker = new WeightedRandomPicker<>();
			Set<StarSystemAPI> populatedSystems = new HashSet<>();
			for (MarketAPI market : sector.getEconomy().getMarketsCopy())
			{
				if (market.getFaction().isHostileTo(Factions.INDEPENDENT))
					continue;
				StarSystemAPI system = market.getStarSystem();
				if (system != null)	populatedSystems.add(system);
			}
			
			for (StarSystemAPI location : sector.getStarSystems())
			{
				float weight = 100;
				if (!populatedSystems.contains(location))
					weight = 0.00001f;
				for (PlanetAPI planet : location.getPlanets())
				{
					if (planet.isStar()) continue;
					if (planet.getMarket() != null && !planet.getMarket().isPlanetConditionMarketOnly()) continue;
					if (isNearExistingMarket(planet)) continue;
					
					picker.add(planet, weight);
				}
				for (CampaignTerrainAPI terrain : location.getTerrainCopy())
				{
					// TODO
					if (!ALLOWED_STATION_TERRAIN.contains(terrain.getId()))
						continue;
					if (terrain.getOrbitFocus().getMarket() != null && terrain.getOrbitFocus().getMarket().isPlanetConditionMarketOnly())
						continue;
					if (isNearExistingMarket(terrain)) continue;
					picker.add(terrain);
				}
			}
			toOrbit = picker.pick();
			if (toOrbit instanceof CampaignTerrainAPI)
			{
				orbitRadius = Misc.getDistance(toOrbit.getLocation(), toOrbit.getOrbitFocus().getLocation());
				orbitPeriod = toOrbit.getOrbit().getOrbitalPeriod();
				toOrbit = toOrbit.getOrbitFocus();
			}
			else
			{
				orbitRadius = toOrbit.getRadius() + 200;
				orbitPeriod = SystemGenUtils.getOrbitalPeriod(toOrbit, orbitRadius);
			}
		}
		
		if (toOrbit == null)
		{
			Global.getLogger(FoundationSectorGen.class).warn("Unable to find suitable location for " + FOUNDATION_HQ_NAME);
			sector.getEconomy().removeMarket(foundationBase.getMarket());
			return;
		}
		else
		{
			Global.getLogger(FoundationSectorGen.class).info("Spawning " + FOUNDATION_HQ_NAME + " around " + toOrbit.getName());
		}
		
		// move base from hyperspace to the actual location
		foundationBase.getContainingLocation().removeEntity(foundationBase);
		toOrbit.getContainingLocation().addEntity(foundationBase);
		
		foundationBase.setCircularOrbitPointingDown(toOrbit, 180, orbitRadius, orbitPeriod);
		foundationBase.setInteractionImage("illustrations", "city_from_above");
		if (toOrbit == corvus)
			foundationBase.setCustomDescriptionId("foundation_station_lawson");
		else foundationBase.setCustomDescriptionId("foundation_station_lawson_nojangala");
	}
}
