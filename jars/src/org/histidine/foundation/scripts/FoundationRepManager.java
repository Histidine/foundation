/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.histidine.foundation.scripts;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.BaseCampaignEventListener;
import com.fs.starfarer.api.campaign.FactionAPI;
import com.fs.starfarer.api.campaign.SectorAPI;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.characters.ImportantPeopleAPI;
import com.fs.starfarer.api.characters.PersonAPI;
import com.fs.starfarer.api.impl.campaign.ids.Conditions;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.impl.campaign.ids.Ranks;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import org.apache.log4j.Logger;
import org.histidine.foundation.scripts.world.FoundationSectorGen;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Rattlesnark
 */
public class FoundationRepManager extends BaseCampaignEventListener {
	
	public static final int MIN_SIZE_FOR_FOUNDATION_REP = 5;
	public static Logger log = Global.getLogger(FoundationRepManager.class);
	public static final Set<String> NO_REP_FACTIONS = new HashSet<>();
	public static final String NO_REP_FACTIONS_LIST_PATH = "data/config/foundation/noRepFactions.csv";
	public static final String NO_REP_KEY = "$noFoundationRepresentative";
	
	static {
		// some factions shouldn't have Foundation representatives even if they otherwise qualify
		try {
			JSONArray charCSV = Global.getSettings().getMergedSpreadsheetDataForMod("faction", NO_REP_FACTIONS_LIST_PATH, "SteinerFoundation");
			for(int x = 0; x < charCSV.length(); x++)
			{
				JSONObject row = charCSV.getJSONObject(x);
				String factionId = row.getString("faction");
				NO_REP_FACTIONS.add(factionId);
			}
		} catch (IOException | JSONException ex) {
			log.error(ex);
		}
	}
	
	protected HashMap<MarketAPI, PersonAPI> foundationReps = new HashMap<>();
	
	public FoundationRepManager() {
		super(true);
	}
	
	public boolean shouldMarketHaveRep(MarketAPI market)
	{
		FactionAPI faction = market.getFaction();
		if (market.getMemory().getBoolean(NO_REP_KEY)) 
			return false;
		if (faction.isNeutralFaction() || faction.isPlayerFaction() || faction.isHostileTo(Factions.INDEPENDENT) || NO_REP_FACTIONS.contains(faction.getId()))
			return false;
		if (market.hasCondition(Conditions.DECIVILIZED) || market.hasCondition(Conditions.ABANDONED_STATION)) 
			return false;
		if (market.getSize() < MIN_SIZE_FOR_FOUNDATION_REP && !market.getId().equals(FoundationSectorGen.FOUNDATION_HQ_ID)) 
			return false;
		return true;
	}
	
	public boolean addFoundationRepToMarket(MarketAPI market)
	{
		if (foundationReps.containsKey(market))
		{
			//log.debug("Market " + market.getId() + " already has Foundation rep, not adding");
			return false;
		}
		
		SectorAPI sector = Global.getSector();
		ImportantPeopleAPI ip = sector.getImportantPeople();
		FactionAPI independent = sector.getFaction(Factions.INDEPENDENT);
		PersonAPI rep = independent.createRandomPerson();
		rep.setRankId(Ranks.CITIZEN);
		rep.setPostId("foundationRep");
		rep.getMemory().set("$foundation_isRep", true);

		market.getCommDirectory().addPerson(rep);
		market.addPerson(rep);
		ip.addPerson(rep);
		ip.getData(rep).getLocation().setMarket(market);
		
		foundationReps.put(market, rep);
		log.info("Added Foundation representative to " + market.getId());
		return true;
	}
	
	public boolean removeFoundationRepFromMarket(MarketAPI market)
	{
		if (!foundationReps.containsKey(market))
		{
			//log.debug("Market " + market.getId() + " does not have a Foundation rep to remove");
			return false;
		}
		PersonAPI rep = foundationReps.get(market);
		ImportantPeopleAPI ip = Global.getSector().getImportantPeople();
		market.getCommDirectory().removePerson(rep);
		market.removePerson(rep);
		ip.removePerson(rep);
		
		foundationReps.remove(market);
		log.info("Removed Foundation representative from " + market.getId());
		return true;
	}
	
	public void createFoundationRepresentatives() {

		for (MarketAPI market : Global.getSector().getEconomy().getMarketsCopy()) 
		{
			if (shouldMarketHaveRep(market)) addFoundationRepToMarket(market);
		}
	}

	@Override
	public void reportPlayerOpenedMarket(MarketAPI market) {
		if (shouldMarketHaveRep(market)) addFoundationRepToMarket(market);
		else removeFoundationRepFromMarket(market);
	}
}
