package org.histidine.foundation.scripts;

import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.campaign.CampaignFleetAPI;
import com.fs.starfarer.api.campaign.econ.MarketAPI;
import com.fs.starfarer.api.characters.MutableCharacterStatsAPI;
import com.fs.starfarer.api.characters.PersonAPI;
import com.fs.starfarer.api.characters.SkillSpecAPI;
import com.fs.starfarer.api.impl.campaign.ids.Factions;
import com.fs.starfarer.api.util.Misc;
import java.util.List;
import org.lwjgl.util.vector.Vector2f;

/**
 *
 * @author Rattlesnark
 */
public class UtilFuncs {
	
	public static MarketAPI getClosestMarket()
	{
		List<MarketAPI> markets = Global.getSector().getEconomy().getMarketsCopy();
		CampaignFleetAPI playerFleet = Global.getSector().getPlayerFleet();
		
		MarketAPI closestMarket = null;
		float closestDist = 999999f;
		Vector2f playerLoc = playerFleet.getLocationInHyperspace();
		for (MarketAPI market : markets)
		{
			if (!market.getFactionId().equals(Factions.INDEPENDENT) || market.getFaction().isHostileTo(Factions.PLAYER))
				continue;
			float dist = Misc.getDistance(market.getLocationInHyperspace(), playerLoc);
			if (dist < closestDist)
			{
				closestMarket = market;
				closestDist = dist;
			}
		}
		return closestMarket;
	}

	public static void purgeNonCombatSkills(PersonAPI person) 
	{
		MutableCharacterStatsAPI stats = person.getStats();
		for (String skillId : Global.getSettings().getSortedSkillIds())
		{
			SkillSpecAPI skill = Global.getSettings().getSkillSpec(skillId);
			if (!skill.isCombatOfficerSkill())
				stats.setSkillLevel(skillId, 0f);
		}
	}
}
