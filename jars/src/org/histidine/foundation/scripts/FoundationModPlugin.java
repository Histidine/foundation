package org.histidine.foundation.scripts;

import com.fs.starfarer.api.BaseModPlugin;
import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.impl.campaign.procgen.ProcgenUsedNames;
import exerelin.campaign.SectorManager;
import org.histidine.foundation.scripts.world.FoundationSectorGen;

public class FoundationModPlugin extends BaseModPlugin {

    public static final boolean HAVE_NEXERELIN = Global.getSettings().getModManager().isModEnabled("nexerelin");
    public static final boolean HAVE_RAGS_2_RICHES = Global.getSettings().getModManager().isModEnabled("RagsToRiches");
    
    @Override
    public void onGameLoad(boolean newGame) {
        
        //if (!Global.getSector().getEventManager().isOngoing(null, "foundation_faction_salary")) {
        //    Global.getSector().getEventManager().startEvent(null, "foundation_faction_salary", null);
        //}
        if (!Global.getSector().getEventManager().isOngoing(null, "foundation_insurance")) {
            Global.getSector().getEventManager().startEvent(null, "foundation_insurance", null);
        }
    }
    
    @Override
    public void onNewGame() {
        ProcgenUsedNames.notifyUsed(FoundationSectorGen.FOUNDATION_HQ_NAME);
    }
    
    @Override
    public void onNewGameAfterProcGen()
	{
		new FoundationSectorGen().createFoundationBasePt1(Global.getSector());
	}
	
	@Override
	public void onNewGameAfterEconomyLoad() 
	{
		FoundationSectorGen.createFoundationBasePt2(Global.getSector());
		
		FoundationRepManager manager = new FoundationRepManager();
		Global.getSector().addListener(manager);
		manager.createFoundationRepresentatives();
	}
    
    @Override
    public void onNewGameAfterTimePass() {
        //Global.getSector().addScript(new TutorialMissionCreator());
    }
}
