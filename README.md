# README #

The Steiner Foundation is a mod for the game [Starsector](http://fractalsoftworks.com). It implements a number of features to reduce the difficulty of the campaign.

Current release version: v1.2.1

### Setup instructions ###
Check out the repo to Starsector/mods/Steiner Foundation (or some other folder name) and it can be played immediately. 

Create a project with Steiner Foundation/jars/src as a source folder to compile the Java files.

### License ###
All code is licensed under the [MIT License (Expat License version)](https://opensource.org/licenses/MIT) except where otherwise specified.

The names Mark Steiner, Hartford Lawson and other references to the Starsector story _Hope and Duty_ belong to user Pendragon (A.C. Martindale), all rights reserved.

All other assets are released as [CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/) unless otherwise specified.